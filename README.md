# Themely Marketplace Onboarding #

Our marketplace plugin includes an onboarding experience (Theme Setup Wizard) to speed up and facilitate the setup of WordPress themes. It uses the latest version of MerlinWP (1.0.0) and TGMPA (2.6.1). Theme authors who opt-in to use our Theme Setup Wizard do not have to include MerlinWP or TGMPA with their themes.

### Setup & Configuration Instructions ###

1. Download `themely.php` file.
2. Include the file in your theme's `functions.php` file.
3. Customize the configuration for your theme (look for the `// CUSTOMIZE` indicator inside the file).

**Important**: Don't forget to change the `text-domain` and `prefix_`

### Test Your Configuration ###

1. Download the [Themely Marketplace WordPress plugin](https://themely-wordpress.s3.amazonaws.com/themely.zip).
2. Install and activate the plugin.
3. Install and activate your theme.
4. Run through all steps of the Theme Setup Wizard to make sure it's working properly.

### Questions or Technical Support ###

If you have questions or need help with configuration here's how you can reach us.

[Chat with us on Discord](https://discord.gg/f3m2Pmp)

Send an email to `support@themely.com`

Voice message or text on Whatsapp `+1 (514) 883-0132`

Time Zone: Eastern Standard Time (GMT -4)

Spoken & written languages: English, Français, Español (un poquito)

Office Location: Montreal, Canada