<?php
/**
* Themely Marketplace Onboarding
*/

if ( !class_exists( 'Merlin' ) ) {
	return;
}

/**
* MerlinWP Config
*/
$wizard = new Merlin(
	$config = array(
		'merlin_url'           => 'theme-setup-wizard', // The wp-admin page slug where Merlin WP loads.
		'parent_slug'          => 'themes.php', // The wp-admin parent page slug for the admin menu item.
		'capability'           => 'manage_options', // The capability required for this menu to be displayed to the user.
		'child_action_btn_url' => 'https://codex.wordpress.org/child_themes', // URL for the 'child-action-link'.
		'dev_mode'             => false, // Enable development mode for testing.
		'license_step'         => false, // EDD license activation step.
		'license_required'     => false, // Require the license activation step.
		'license_help_url'     => '', // URL for the 'license-tooltip'.
		'edd_remote_api_url'   => '', // EDD_Theme_Updater_Admin remote_api_url.
		'edd_item_name'        => '', // EDD_Theme_Updater_Admin item_name.
		'edd_theme_slug'       => '', // EDD_Theme_Updater_Admin item_slug.
		'ready_big_button_url' => esc_url(get_home_url()), // Link for the big button on the ready step.
	),
	$strings = array(
		'admin-menu'               => esc_html__( 'Theme Setup Wizard', 'text-domain' ),
		/* translators: 1: Title Tag 2: Theme Name 3: Closing Title Tag */
		'title%s%s%s%s'            => esc_html__( '%1$s%2$s Themes &lsaquo; Theme Setup Wizard for %3$s%4$s', 'text-domain' ),
		'return-to-dashboard'      => esc_html__( 'Return to the dashboard', 'text-domain' ),
		'ignore'                   => esc_html__( 'Disable this wizard', 'text-domain' ),
		'btn-skip'                 => esc_html__( 'Skip', 'text-domain' ),
		'btn-next'                 => esc_html__( 'Next', 'text-domain' ),
		'btn-start'                => esc_html__( 'Start', 'text-domain' ),
		'btn-no'                   => esc_html__( 'Cancel', 'text-domain' ),
		'btn-plugins-install'      => esc_html__( 'Install', 'text-domain' ),
		'btn-child-install'        => esc_html__( 'Install', 'text-domain' ),
		'btn-content-install'      => esc_html__( 'Install', 'text-domain' ),
		'btn-import'               => esc_html__( 'Import', 'text-domain' ),
		'btn-license-activate'     => esc_html__( 'Activate', 'text-domain' ),
		'btn-license-skip'         => esc_html__( 'Later', 'text-domain' ),
		/* translators: Theme Name */
		'license-header%s'         => esc_html__( 'Activate %s', 'text-domain' ),
		/* translators: Theme Name */
		'license-header-success%s' => esc_html__( '%s is Activated', 'text-domain' ),
		/* translators: Theme Name */
		'license%s'                => esc_html__( 'Enter your license key to enable remote updates and theme support.', 'text-domain' ),
		'license-label'            => esc_html__( 'License key', 'text-domain' ),
		'license-success%s'        => esc_html__( 'The theme is already registered, so you can go to the next step!', 'text-domain' ),
		'license-json-success%s'   => esc_html__( 'Your theme is activated! Remote updates and theme support are enabled.', 'text-domain' ),
		'license-tooltip'          => esc_html__( 'Need help?', 'text-domain' ),
		/* translators: Theme Name */
		'welcome-header%s'         => esc_html__( 'Welcome to %s', 'text-domain' ),
		'welcome-header-success%s' => esc_html__( 'Hi! Welcome back', 'text-domain' ),
		'welcome%s'                => esc_html__( 'This wizard will set up your theme, install plugins, and import content. It is optional & should take only a few minutes.', 'text-domain' ),
		'welcome-success%s'        => esc_html__( 'You may have already run this theme setup wizard. If you would like to proceed anyway, click on the "Start" button below.', 'text-domain' ),
		'child-header'             => esc_html__( 'Install Child Theme', 'text-domain' ),
		'child-header-success'     => esc_html__( 'You\'re good to go!', 'text-domain' ),
		'child'                    => esc_html__( 'Let\'s build & activate a child theme so you may easily make theme changes.', 'text-domain' ),
		'child-success%s'          => esc_html__( 'Your child theme has already been installed and is now activated, if it wasn\'t already.', 'text-domain' ),
		'child-action-link'        => esc_html__( 'Learn about child themes', 'text-domain' ),
		'child-json-success%s'     => esc_html__( 'Awesome. Your child theme has already been installed and is now activated.', 'text-domain' ),
		'child-json-already%s'     => esc_html__( 'Awesome. Your child theme has been created and is now activated.', 'text-domain' ),
		'plugins-header'           => esc_html__( 'Install Plugins', 'text-domain' ),
		'plugins-header-success'   => esc_html__( 'You\'re up to speed!', 'text-domain' ),
		'plugins'                  => esc_html__( 'Let\'s install some essential WordPress plugins to get your site up to speed.', 'text-domain' ),
		'plugins-success%s'        => esc_html__( 'The required WordPress plugins are all installed and up to date. Press "Next" to continue the setup wizard.', 'text-domain' ),
		'plugins-action-link'      => esc_html__( 'Advanced', 'text-domain' ),
		'import-header'            => esc_html__( 'Import Content', 'text-domain' ),
		'import'                   => esc_html__( 'Let\'s import content to your website, to help you get familiar with the theme.', 'text-domain' ),
		'import-action-link'       => esc_html__( 'Advanced', 'text-domain' ),
		'ready-header'             => esc_html__( 'All done. Have fun!', 'text-domain' ),
		/* translators: Theme Author */
		'ready%s'                  => esc_html__( 'Your theme has been set up. Enjoy your new theme by %s.', 'text-domain' ),
		'ready-action-link'        => esc_html__( 'Extras', 'text-domain' ),
		'ready-big-button'         => esc_html__( 'View Your Website', 'text-domain' ), // CUSTOMIZE: Ready Big Button Text
		'ready-link-1'             => sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://support.example.com/documentation/', esc_html__( 'Theme Documentation', 'text-domain' ) ), // CUSTOMIZE: Link to theme documentation
		'ready-link-2'             => sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://support.example.com/forums/', esc_html__( 'Support Forum', 'text-domain' ) ), // CUSTOMIZE: Link to theme support page
		'ready-link-3'             => sprintf( '<a href="%1$s">%2$s</a>', esc_url(wp_customize_url()), esc_html__( 'Customize Your Website', 'text-domain' ) ), // Link to Customizer
	)
);

/**
* Merlin WP Filters.
*/

/**
 * CUSTOMIZE: Define the demo import files (remote files).
 * To define imports, you just have to add the following code structure,
 * with your own values to your theme (using the 'merlin_import_files' filter).
 * Remove this if you don't need it.
 */
function prefix_merlin_import_files() {
	return array(
		array(
			'import_file_name'           => 'Demo Import 1',
			'import_file_url'            => 'https://www.example.com/merlin/demo-content.xml',
			'import_widget_file_url'     => 'https://www.example.com/merlin/widgets.json',
			'import_customizer_file_url' => 'https://www.example.com/merlin/customizer.dat',
			'import_redux'               => array(
				array(
					'file_url'    => 'https://www.example.com/merlin/redux_options.json',
					'option_name' => 'redux_option_name',
				),
			),
			'import_preview_image_url'   => 'https://www.example.com/merlin/preview_import_image1.jpg',
			'import_notice'              => __( 'A special note for this import.', 'text-domain' ),
			'preview_url'                => 'https://www.example.com/my-demo-1',
		),
		array(
			'import_file_name'           => 'Demo Import 2',
			'import_file_url'            => 'https://www.example.com/merlin/demo-content2.xml',
			'import_widget_file_url'     => 'https://www.example.com/merlin/widgets2.json',
			'import_customizer_file_url' => 'https://www.example.com/merlin/customizer2.dat',
			'import_redux'               => array(
				array(
					'file_url'    => 'https://www.example.com/merlin/redux_options2.json',
					'option_name' => 'redux_option_name2',
				),
			),
			'import_preview_image_url'   => 'https://www.example.com/merlin/preview_import_image2.jpg',
			'import_notice'              => __( 'A special note for this import.', 'text-domain' ),
			'preview_url'                => 'https://www.example.com/my-demo-2',
		),
	);
}
add_filter( 'merlin_import_files', 'prefix_merlin_import_files' );

/**
 * CUSTOMIZE: Define the demo import files (local files).
 * You have to use the same filter as in above example,
 * but with a slightly different array keys: local_*.
 * The values have to be absolute paths (not URLs) to your import files.
 * To use local import files, that reside in your theme folder,
 * please use the below code.
 * Note: make sure your import files are readable!
 * Remove this if you don't need it.
 */
function prefix_merlin_local_import_files() {
	return array(
		array(
			'import_file_name'             => 'Demo Import 1',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo/demo-content.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo/widgets.json',
			'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo/customizer.dat',
			'local_import_redux'           => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'inc/demo/redux_options.json',
					'option_name' => 'redux_option_name',
				),
			),
			'import_preview_image_url'     => 'https://www.example.com/merlin/preview_import_image1.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately.', 'text-domain' ),
			'preview_url'                  => 'https://www.example.com/my-demo-1',
		),
		array(
			'import_file_name'             => 'Demo Import 2',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo/demo-content2.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo/widgets2.json',
			'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo/customizer2.dat',
			'local_import_redux'           => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'inc/demo/redux_options2.json',
					'option_name' => 'redux_option_name2',
				),
			),
			'import_preview_image_url'     => 'https://www.example.com/merlin/preview_import_image2.jpg',
			'import_notice'                => __( 'A special note for this import.', 'text-domain' ),
			'preview_url'                  => 'https://www.example.com/my-demo-2',
		),
	);
}
add_filter( 'merlin_import_files', 'prefix_merlin_local_import_files' );

/**
 * CUSTOMIZE: Execute custom code after the whole import has finished.
 * Set the menu and default front & blog pages
 * Remove this if you don't need it.
 */
function prefix_merlin_after_import_setup() {
	// Assign menus to their locations.
	$main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
	set_theme_mod(
		'nav_menu_locations', array(
			'main-menu' => $main_menu->term_id,
		)
	);
	// Assign front page and posts page (blog page).
	$front_page_id = get_page_by_title( 'Home' );
	$blog_page_id  = get_page_by_title( 'Blog' );
	update_option( 'show_on_front', 'page' );
	update_option( 'page_on_front', $front_page_id->ID );
	update_option( 'page_for_posts', $blog_page_id->ID );
}
add_action( 'merlin_after_all_import', 'prefix_merlin_after_import_setup' );

/**
 * CUSTOMIZE: Remove child theme step
 * Remove this if you don't need it.
 */
add_filter( wp_get_theme( )->template . '_merlin_steps', function( $steps ) {
	unset( $steps['child'] );
	return $steps;
});

/**
* CUSTOMIZE: TGMPA Config
* Remove this if you don't need it.
*/
add_action( 'tgmpa_register', 'prefix_register_required_plugins' );
function prefix_register_required_plugins() {
	$plugins = array(
		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'TGM Example Plugin', // The plugin name.
			'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
			'source'             => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
			'name'         => 'TGM New Media Plugin', // The plugin name.
			'slug'         => 'tgm-new-media-plugin', // The plugin slug (typically the folder name).
			'source'       => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', // The plugin source.
			'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
		),
		// This is an example of how to include a plugin from a GitHub repository in your theme.
		// This presumes that the plugin code is based in the root of the GitHub repository
		// and not in a subdirectory ('/src') of the repository.
		array(
			'name'   => 'Adminbar Link Comments to Pending',
			'slug'   => 'adminbar-link-comments-to-pending',
			'source' => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		),
		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'     => 'BuddyPress',
			'slug'     => 'buddypress',
			'required' => false,
		),
		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have Yoast SEO installed *or* Yoast SEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.
		array(
			'name'        => 'Yoast SEO',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
		),
	);
	$config = array(
		'id'           => 'tgmpa',				   // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                    // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);
	tgmpa($plugins, $config);
}

/**
* CUSTOMIZE: Redirect to Theme Setup Wizard after theme activation.
* Remove this if you don't need it.
*/
function prefix_redirect_after_activation() {
	wp_safe_redirect(admin_url('themes.php?page=theme-setup-wizard'));
}
add_action( 'after_switch_theme', 'prefix_redirect_after_activation' );